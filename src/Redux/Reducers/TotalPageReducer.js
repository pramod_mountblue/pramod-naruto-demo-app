import { SET_TOTAL_PAGE } from "../ReduxConstants/ReduxConstants";

const totalPageReducer = (state=null,action) => {
    switch (action.type) {
        case SET_TOTAL_PAGE:
                return action.data;
        default:
            return state;
    }
}

export default totalPageReducer