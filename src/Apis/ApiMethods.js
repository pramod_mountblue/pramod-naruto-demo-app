const proxyUrl = 'https://cors-anywhere.herokuapp.com/'
const baseUrl = "https://api.jikan.moe/";

export const get = (url) => {
    const headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
    };
    const options = {
        method: "GET",
        headers: headers,
    };
    return fetch(proxyUrl + baseUrl + url, options).then(
        (res) => (res.status === 200 ? res.json() : null),
        (err) => err
    );
}
