import { get } from "./ApiMethods.js";


export const getData = async (query, page) => {
    const res = await get(`v3/search/anime?q=${query}&limit=16&page=${page}`);
    return res.results ? res.results : [];
};
