import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getData } from '../../Apis/Apis'
import { setTotalPage } from '../../Redux/Actions/ActionCreators';
import './Home.css'


const Home = ({ totalPage, setTotalPage }) => {
  const [ data, setData ] = useState([])
  const [ searchText, setSearchText ] = useState('naruto')
  const [ dataLoading, setDataLoading ] = useState(false)
  const [ loadingMore, setLoadingMore ] = useState(false)


  useEffect(() => {
      (async () => {
        if (data.length) setLoadingMore(true)
        else setDataLoading(true)
        const newData = await getData(searchText, totalPage)
        const allData = [ ...data, ...newData ]
        setData(allData)
        if (allData.length > 16) setLoadingMore(false)
        else setDataLoading(false)
      })()
  }, [totalPage]);


  const title = (title) => title.length > 20 ? title.slice(0, 20) + '...' : title

  const narutos = () => {
    const narutos = data.map((datum, index) => 
        <div key={index}>
            <img src={datum.image_url} className='image' />
            <div className='title'>{title(datum.title)}</div>
        </div>
    )
    return narutos
  }

  const search = () => (
    <div>
        <input type="text" name="name" id="id" placeholder="search for an anime, e.g Naruto" className='input'
            value={searchText} onChange={(e)=>handleChange(e.target.value)} onKeyDown={(e)=>searchData(e.key)}
        />
        <button className='button' onClick={()=>searchData('searchData')}>Go</button>
    </div>
  )

  const handleChange = (searchText) => setSearchText(searchText)

  const searchData = async (action) => {
      if (action==='Enter' || action==='searchData') {
        setDataLoading(true)
        const data = await getData(searchText, totalPage)
        setData(data)
        setDataLoading(false)
      }
  }

  const loadMore = () => !loadingMore && setTotalPage(totalPage + 1)

  const loader = () => <div className={`loader ${loadingMore && 'load-more-loader'}`}></div> ;


  return ( 
    <div className='container'>
      <div className='input-container'>{search()}</div>
      {!dataLoading ? data.length > 0 ? 
        <div className='narutos'>{narutos()}</div> 
        : 
        <div className='no-data-found'>No Data Found</div> 
        : 
        <div className='loader-container'>{loader()}</div>
      }
      {data.length > 0 && !dataLoading && 
        <div className='load-more' onClick={loadMore}>
          { !loadingMore ? 'Load More...' : loader()}
        </div> 
      }
    </div>
   );
}


export default  withRouter(connect((state) => {
  const { totalPage } = state
  return {
      totalPage
  }
},{
    setTotalPage
})(Home))