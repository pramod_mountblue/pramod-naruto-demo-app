const baseUrl="https://nodeapi.gomechanic.in/api/car_parts/"


 export function get(url){
    const options={
        method:"GET",
    }
    return fetch((baseUrl+url), options).then(res=> res.status===200 ? res.json() : null, err=>err);
}

export function post(url,data){
    const options={
        method:"POST",
        body:JSON.stringify(data),
    }
    return fetch(baseUrl+url, options).then(res=>res.json(),err=>err);
}