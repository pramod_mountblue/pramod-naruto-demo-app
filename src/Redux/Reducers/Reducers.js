import { combineReducers } from "redux";
import totalPageReducer from "./TotalPageReducer";

const reducers = combineReducers({
    totalPage: totalPageReducer,
});

export default reducers;
