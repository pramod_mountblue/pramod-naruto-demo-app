import { createStore, applyMiddleware } from 'redux';
import reducers from '../Reducers/Reducers';
// import logger from 'redux-logger'
// window._REDUX_DEVTOOLS_EXTENSION_ && window._REDUX_DEVTOOLS_EXTENSION_()


const initialState = {
    totalPage: 1,
}

const store = createStore(reducers, initialState)

export default store
    