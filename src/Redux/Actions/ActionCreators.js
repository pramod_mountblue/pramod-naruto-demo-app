import { SET_TOTAL_PAGE } from "../ReduxConstants/ReduxConstants";
  

export const setTotalPage = (data) => {
  return { type: SET_TOTAL_PAGE, data: data }
}
